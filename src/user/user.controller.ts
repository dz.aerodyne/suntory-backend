import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Query, Res } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('')
  async getAll(@Res() res) {
    const data = await this.userService.getAll();
    return res.status(HttpStatus.OK).json(data);
  }

  @Get('/:id')
  async get(@Res() res, @Param('id') id) {
    const data = await this.userService.get(id);
    if (!data) throw new NotFoundException('Project does not exist!');
    return res.status(HttpStatus.OK).json(data);
  }

  @Post('/create')
  async add(@Res() res, @Body() dto: CreateUserDto) {
    const data = await this.userService.create(dto);
    return res.status(HttpStatus.OK).json({
      message: 'Project has been created successfully',
      data,
    });
  }

  @Put('/update')
  async update(@Res() res, @Query('id') id, @Body() dto: CreateUserDto) {
    const customer = await this.userService.update(id, dto);
    if (!customer) throw new NotFoundException('Project does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'Project has been successfully updated',
      customer,
    });
  }

  @Delete('/delete')
  async delete(@Res() res, @Query('id') id) {
    const customer = await this.userService.delete(id);
    if (!customer) throw new NotFoundException('Project does not exist');
    return res.status(HttpStatus.OK).json({
      message: 'Project has been deleted',
      customer,
    });
  }
}
