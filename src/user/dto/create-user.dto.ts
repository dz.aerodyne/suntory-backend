export class CreateUserDto {
  readonly name: String
  readonly email: String
  readonly password: String
  readonly created_at: String
}