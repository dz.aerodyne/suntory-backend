import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose'
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './interface/user.interface';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private readonly model: Model<User>,
  ) {}

  async getAll(): Promise<User[]> {
    const items = await this.model.find().exec();
    return items;
  }

  async get(id): Promise<User> {
    const item = await this.model.findById(id).exec();
    return item;
  }

  async create(dto: CreateUserDto): Promise<User> {
    const item = await this.model(dto);
    return item.save();
  }

  async update(id, dto: CreateUserDto): Promise<User> {
    const item = await this.model.findByIdAndUpdate(id, dto, {
      new: true,
    });
    return item;
  }

  async delete(id): Promise<any> {
    const item = await this.model.findByIdAndRemove(id);
    return item;
  }
}
