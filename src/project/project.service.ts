import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Project } from './interface/project.interface';
import { CreateProjectDto } from './dto/create-project.dto';

@Injectable()
export class ProjectService {
  constructor(
    @InjectModel('Project') private readonly model: Model<Project>,
  ) {}

  async getAll(): Promise<Project[]> {
    const items = await this.model.find().exec();
    return items;
  }

  async get(id): Promise<Project> {
    const item = await this.model.findById(id).exec();
    return item;
  }

  async create(dto: CreateProjectDto): Promise<Project> {
    const item = await this.model(dto);
    return item.save();
  }

  async update(id, dto: CreateProjectDto): Promise<Project> {
    const item = await this.model.findByIdAndUpdate(id, dto, {
      new: true,
    });
    return item;
  }

  async delete(id): Promise<any> {
    const item = await this.model.findByIdAndRemove(id);
    return item;
  }
}
