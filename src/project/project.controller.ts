import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectService } from './project.service';

@Controller('project')
export class ProjectController {
  constructor(private projectService: ProjectService) {}

  @Get('')
  async getAll(@Res() res) {
    const data = await this.projectService.getAll();
    return res.status(HttpStatus.OK).json(data);
  }

  @Get('/:id')
  async get(@Res() res, @Param('id') id) {
    const data = await this.projectService.get(id);
    if (!data) throw new NotFoundException('Project does not exist!');
    return res.status(HttpStatus.OK).json(data);
  }

  @Post('/create')
  async add(@Res() res, @Body() dto: CreateProjectDto) {
    const data = await this.projectService.create(dto);
    return res.status(HttpStatus.OK).json({
      message: 'Project has been created successfully',
      data,
    });
  }

  @Put('/update')
  async update(@Res() res, @Query('id') id, @Body() dto: CreateProjectDto) {
    const customer = await this.projectService.update(id, dto);
    if (!customer) throw new NotFoundException('Project does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'Project has been successfully updated',
      customer,
    });
  }

  @Delete('/delete')
  async delete(@Res() res, @Query('id') id) {
    const customer = await this.projectService.delete(id);
    if (!customer) throw new NotFoundException('Project does not exist');
    return res.status(HttpStatus.OK).json({
      message: 'Project has been deleted',
      customer,
    });
  }
}
