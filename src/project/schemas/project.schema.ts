import * as mongoose from 'mongoose'

export const ProjectSchema = new mongoose.Schema({
  name: String,
  description: String,
  created_at: { type: Date, default: Date.now }
})