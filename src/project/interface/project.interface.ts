import { Document } from 'mongoose'

export interface Project extends Document {
  readonly name: String,
  readonly description: String,
  readonly created_at: Date
}