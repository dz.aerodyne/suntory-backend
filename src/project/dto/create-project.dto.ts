export class CreateProjectDto {
  readonly name: String
  readonly description: String
  readonly created_at: String
}